const scroll = () => {
    document.querySelector('#game').scrollIntoView({
        behavior: 'smooth'
    });
}

document.querySelector('.scroll').addEventListener('click', scroll);

const gameSummery = {
    numbers: 0,
    wins: 0,
    losses: 0,
    draws: 0
};

const game = {
    playerHand: '',
    aiHand: ''
};

const hands = [...document.querySelectorAll('.select img')];

// Fn Player Hand/Choice

function handSelection() {
    game.playerHand = this.dataset.option;
    hands.forEach(hand => hand.style.boxShadow = '');
    this.style.boxShadow = '0px 36px 37px -24px rgba(64,167,152,1)';
}

// Fn AI Choice

function aiChoice() {
    const aiHand = hands[Math.floor(Math.random() * 3)].dataset.option;
    return aiHand;
}

// Fn Checking Result

function checkResult(player, ai) {
    if (player === ai) {
        return 'draw'
    } else if ((player === 'paper' && ai === 'rock') || (player === 'rock' && ai === 'scissors') || (player === 'scissors' && ai === 'paper')) {
        return 'win'
    } else {
        return 'losses'
    }
}

// Fn Publish Result

function publishResult(player, ai, result) {
    document.querySelector('[data-summary="your-choice"]').textContent = player;
    document.querySelector('[data-summary="ai-choice"]').textContent = ai;
    document.querySelector('p.numbers span').textContent = ++gameSummery.numbers;

    if (result === 'win') {
        document.querySelector('p.wins span').textContent = ++gameSummery.wins;
        document.querySelector('[data-summary="who-win"]').textContent = 'PLAYER WINS';
        document.querySelector('[data-summary="who-win"]').style.color = '#40a798';
    } else if (result === 'losses') {
        document.querySelector('p.losses span').textContent = ++gameSummery.losses;
        document.querySelector('[data-summary="who-win"]').textContent = 'AI WINS';
        document.querySelector('[data-summary="who-win"]').style.color = '#d63838';
    } else {
        document.querySelector('p.draws span').textContent = ++gameSummery.draws;
        document.querySelector('[data-summary="who-win"]').textContent = 'DRAW';
        document.querySelector('[data-summary="who-win"]').style.color = '#5151ca';
    }
}

// Fn End Game

function endGame() {
    document.querySelector(`[data-option="${game.playerHand}"]`).style.boxShadow = '';
    game.playerHand = '';
}

// Fn Start

function startGame() {
    if (!game.playerHand) {
        return alert('CHOOSE SOMETHING');
    }
    game.aiHand = aiChoice();
    const gameResult = checkResult(game.playerHand, game.aiHand);
    publishResult(game.playerHand, game.aiHand, gameResult)

    endGame();
};

hands.forEach(hand => hand.addEventListener('click', handSelection));

document.querySelector('.start').addEventListener('click', startGame);